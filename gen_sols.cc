#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <set>
#include <sstream>
#define abs(x) (x < 0 ? -x : x)
using namespace std;
typedef vector< vector<int> > mtx;

const vector<int> dim = {1,3,3,4,5,2,2,4,6};

long long p = 37;
long long MOD = 1000000007;
set<long long> seen;
bool did_solve=false;

void solve(const vector<int> &L, const vector<int> &R, const mtx &M, const vector<int> &fL, const vector<int> &fR) {
	long long p_pow = 1;
	long long hash = 0;
	for (int i = 0; i < 9; ++i) {
		for (int j = 0; j < 9; ++j) {
			hash = (hash + p_pow * M[i][j])%MOD;
			p_pow = (p_pow * p) % MOD;
		}
	}
	if (seen.find(hash) != seen.end())
		return;
	seen.insert(hash);

	bool done=true;
	for (int i = 0; i < 9; ++i) {
		if (L[i] != fL[i] || R[i] != fR[i]) {
			done=false;
			break;
		}
	}
	if (done) {
		for (int i = 0; i < 9; ++i) {
			for (int j = 0; j < 9; ++j) {
				cout << M[i][j] << "\t";
			}
			cout << "\n";
		}
		did_solve=true;
		cout << "=========DONE==========\n";
		return;
	}
	
	for (int i = 0; i < 9; ++i) {
		for (int j = 0; j < 9; ++j) {
			if (fL[i] + dim[j] <= L[i] && fR[j] + dim[i] <= R[j]) {
				vector<int> fL2 = fL;
				vector<int> fR2 = fR;
				mtx M2 = M;
				fL2[i] += dim[j];
				fR2[j] += dim[i];
				++M2[i][j];
				solve(L,R,M2,fL2,fR2);
			}
		}
	}
}

int main(int argc, char *argv[]) {
	vector< pair<int, pair< vector<int>, vector<int> > > > solutions;
	int r = 13,s=120;
	if (argc >= 2) {
		istringstream ss{argv[1]};
		ss >> r;
	}
	if (argc >= 3) {
		istringstream ss{argv[1]};
		ss >> r;
		istringstream ss2{argv[2]};
		ss2 >> s;
	}

	
	for (int m0 = 0; m0 < r; ++m0) {
		for (int m1 = 0; m1 < r; ++m1) {
			for (int m2 = 0; m2 < r; ++m2) {
				for (int m3 = 0; m3 < r; ++m3) {
					for (int m4 = 0; m4 < r; ++m4) {
						int ord4 = m0-m1-m2+m4;
						if (m0 + 3*(m1+m2) + 4*m3 + 5*m4 >= s)
							continue;
						if (abs(ord4) > 1)
							continue;
						for (int m5 = 0; m5 < r; ++m5) {
							if (m0 + 3*(m1+m2) + 4*m3 + 5*m4 +2*m5 >= s)
								continue;
							for (int m6 = 0; m6 < r; ++m6) {
								if (m0 + 3*(m1+m2) + 4*m3 + 5*m4 +2*(m5+m6) >= s)
									continue;
								for (int m7 = 0; m7 < r; ++m7) {
									if (m0 + 3*(m1+m2) + 4*m3 + 5*m4 +2*(m5+m6) +4*m7 >= s)
										continue;
									
									int ord6a = m0+m3-m4-m5-m6+m7;
									int ord6b = m0+m3-m4+m5+m6-m7;
									if (abs(ord6a) > 1 || abs(ord6b) > 1)
										continue;
									for (int m8 = 0; m8 < r; ++m8) {
										if (m0 + 3*(m1+m2) + 4*m3 + 5*m4 +2*(m5+m6)+4*m7+6*m8 >= s)
											continue;
										
										
										int ord10aai = m0+m2-m3+m6+m7-m8;
										int ord10aat = m1-m2+m5-m6;
										int ord10abi = m0+m2-m3-m6-m7+m8;
										int ord10abt = m1-m2-m5+m6;
										int ord10bai = m0+m1-m3+m5+m7-m8;
										int ord10bat = -m1+m2-m5+m6;
										int ord10bbi = m0+m1-m3-m5-m7+m8;
										int ord10bbt = -m1+m2+m5-m6;
										int dim = m0+3*(m1+m2) +4*m3+5*m4+\
												  2*(m5+m6) + 4*m7 + 6*m8;
										vector<int> mults{m0,m1,m2,m3,m4,m5,m6,m7,m8};
										vector<int> ords{ord4,ord6a,ord6b,ord10aai,ord10aat,ord10abi,ord10abt,ord10bai,ord10bat,ord10bbi,ord10bbt};
										if (abs(ord4) <= 1 && 
											abs(ord6a) <= 1 && abs(ord6b) <= 1 &&
											abs(ord10aai) <= 1 && abs(ord10aat) == 0 &&
											abs(ord10abi) <= 1 && abs(ord10abt) <= 1 &&
											abs(ord10bai) <= 1 && abs(ord10bat) <= 1 &&
											abs(ord10bbi) <= 1 && abs(ord10bbt) <= 1 &&
											1) {
											solutions.push_back(make_pair(dim, make_pair(mults,ords)));
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	sort(solutions.begin(), solutions.end());
	for (auto x : solutions) {
		cout << x.first << " ++ ";
		for (auto y : x.second.first) {
			cout << y << "\t";
		}
		cout << " ++ ";
		for (auto y : x.second.second) {
			cout << y << "\t";
		}
		cout << "\n";
	}
	cout << flush;

	mtx M;
	for (int i = 0; i < 9; ++i) {
		M.push_back(vector<int>{});
		for (int j = 0; j < 9; ++j) {
			M[i].push_back(0);
		}
	}
	
	cout << solutions.size() << " possible representations of dimension < " << s << endl;
	cout << endl << endl;
	cout << "================================" << endl;
	cout << "====STARTING SOLUTION SEARCH====" << endl;
	cout << "================================" << endl;
	cout << endl;
	for (int i = 1; i < solutions.size(); ++i) {
		int j = i-1;
		while (++j < solutions.size()) {
			if (solutions[i].first != solutions[j].first) {
				break;
			}
			did_solve=false;
			vector<int> fL = {0,0,0,0,0,0,0,0,0};
			vector<int> fR = {0,0,0,0,0,0,0,0,0};
			solve(solutions[i].second.first, solutions[j].second.first, M, fL, fR);
			if (did_solve) {
				string foo;
				cout << "Found solution(s) for i=" << i << ", j=" << j << "; take a look." << endl;
				cout << "L = ";
				for (int k = 0; k < 9; ++k) {
					cout << solutions[i].second.first[k] << " ";
				}
				cout << "\nR = ";
				for (int k = 0; k < 9; ++k) {
					cout << solutions[j].second.first[k] << " ";
				}
				cout << endl;
				cout << "DIM = " << solutions[i].first << endl;
				cout << "Press <ENTER> to continue searching." << flush;
				getline(cin,foo);
			}
			seen.clear();
		}
	}
	cout << "Done with this set of representations." << endl;
}