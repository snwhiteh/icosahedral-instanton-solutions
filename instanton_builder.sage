###################################################################################################
###################################################################################################

# Set q=-2 for 2' (maps coming from copies of 4') or q=2 for 2 (maps coming from copies of 1 and 3)
# Which one you want will depend on the dimension you choose. Don't worry, it'll tell you if you
# should probably be using the other one.
print("Enter the dimension of the rep and optionally a space followed by 2 or -2 for Q. ")
print("If your number is not prime, enter it like N:P where N is" + \
        " the number and P is an odd prime factor of N.")
print("This will tell the system to try to use N/P copies of the P-diml SU(2) irrep.")
inp = input("Please enter your dimension: ")
tok = inp.split(' ')
if ':' in tok[0]:
	REP_DIM = int(tok[0].split(':')[1])
	REP_MUL = int(int(tok[0].split(':')[0])/REP_DIM)
else:
	REP_DIM = int(tok[0])
	REP_MUL = 1

q = -2
if len(tok) > 1:
    q = int(tok[1])


# As discussed, it's probably best to keep this at an odd prime number.

###################################################################################################
###################################################################################################





import warnings
warnings.filterwarnings("ignore", category=UserWarning)
var('x')

# This widget computes decompositions of restrictions of SU2 irreps to the binary icosahedral group
# Most examples we care about are in the comments below
Theta = [0, pi, 2*pi/4, 2*pi/3, 2*pi/6,2*pi/10, 2*2*pi/5, 6*pi/10, 2*pi/5] 
ThetaP = [0, pi, 2*pi/4, 2*pi/3, 2*pi/6,6*pi/10, 2*pi/5,  2*pi/10,2*2*pi/5]
char = lambda a: [ float(limit(sin(a*x)/sin(x), x=t)) for t in Theta ]
charP = lambda a: [ float(limit(sin(a*x)/sin(x), x=t)) for t in ThetaP ]
charP4 = [4, 4, 0, 1, 1, -1, -1, -1, -1]


times = lambda x,y: [a*b for (a,b) in zip(x,y)]
add = lambda x,y: [a+b for (a,b) in zip(x,y)]
sub = lambda x, y: [ a-b for (a,b) in zip(x,y) ]
CCsize = [1,1,30,20,20,12,12,12,12]

num_decomp = lambda x, y: int(round(1/120*sum([ float(a*b*c) for (a,b,c) in zip(x,y,CCsize) ])))
irreps = [char(1),char(2),char(3),char(4),char(5),char(6),charP(3),charP4,charP(2)]
irrep_type = [1,2,3,4,5,6,-3,-4,-2]
decomp = lambda x: [ num_decomp(x, y) for y in irreps ]

def decomp2(x):
    dec = []
    for (irrep, c) in zip(decomp(x), irrep_type):
        dec.extend([c]*irrep)
    return dec



Q.<i,j,k> = QuaternionAlgebra(SR,-1,-1)
tau = (1+sqrt(5))/2
U2 = -1/2*(i+tau*j-tau^-1*k)
rho3 = [ Matrix([ [1,0,0],[0,-1,0],[0,0,-1]]),Matrix([ [-1,0,0],[0,1,0],[0,0,-1]]), 
         -1/2 * Matrix( [ [1,-tau,tau^-1], [-tau,-tau^-1,1], [tau^-1,1,tau]]) ]
rho3P = [ Matrix([ [1,0,0],[0,-1,0],[0,0,-1]]),Matrix([ [-1,0,0],[0,1,0],[0,0,-1]]),
         -1/2 * Matrix( [ [1,tau^-1,-tau], [tau^-1,tau,1], [-tau,1,-tau^-1]]) ]
rho4P = [Matrix([[1,0,0,0], [0,1,0,0], [0,0,-1,0], [0,0,0,-1]]),
         Matrix([[1,0,0,0], [0,-1,0,0], [0,0,1,0], [0,0,0,-1]]),
         1/4*Matrix([ [-1, sqrt(5),-sqrt(5),-sqrt(5)], [sqrt(5),3,1,1],
                       [-sqrt(5),1,-1,3], [-sqrt(5),1,3,-1]   ])]
rho5 = [Matrix([[1,0,0,0,0],[0,1,0,0,0],[0,0,1,0,0],[0,0,0,-1,0],[0,0,0,0,-1]]).dense_matrix(),
        Matrix([[1,0,0,0,0],[0,-1,0,0,0],[0,0,1,0,0],[0,0,0,-1,0],[0,0,0,0,1]]).dense_matrix(),
        1/4*Matrix([ [-1, sqrt(2), -sqrt(3), sqrt(2), -sqrt(8)], [sqrt(2),0,-sqrt(6),2,2], 
                    [-sqrt(3), -sqrt(6), 1,sqrt(6),0], [sqrt(2), 2, sqrt(6),2,0],
                     [-sqrt(8),2,0,0,2] ])]

# Some functions to work with quaternions, coeffs turns a quaternion into a list of its e, i, j, k components
# evalf works like it does in Maple on quaternions
coeffs = lambda x:x.matrix()[0]
evalf = lambda x: sum([a*b for (a,b) in zip([ y._convert(RR) for y in coeffs(x) ], [1,i,j,k])])
norm = lambda x: sum([ a*b for (a,b) in zip(coeffs(x), coeffs(x))])
evalfm = lambda mm: Matrix([ [evalf(x) for x in y ] for y in mm])
projreal = lambda mm: Matrix([ [coeffs(x)[0] for x in y ] for y in mm])
projim = lambda mm: mm-projreal(mm)
conjugate = lambda x: sum([a*b for (a,b) in zip(coeffs(x), [1,-i,-j,-k])])
conjugate_transpose = lambda M: matrix([ [ conjugate(x) for x in y ] for y in M ]).transpose()

#Relevant matrices for symmetric instanton construction
B24P = Matrix([[1,i,j,k]])
B24Pr = Matrix([[1,0,0,0]])
B24Pi = Matrix([[0,i,j,k]]) #split into real and imaginary parts so we can take adjoint easily later
B4P2 = B24P.transpose()
B4P2r = B24Pr.transpose()
B4P2i = B24Pi.transpose()
B23 = Matrix([[i,j,k]])
B32 = B23.transpose()
B13 = B23
B31 = B23.transpose()
B53P = Matrix([ [i,j, -2*k], [0, -sqrt(2)*tau^-1 * k, sqrt(2)*tau*j], [-sqrt(3)*i, sqrt(3)*j, 0],
                [sqrt(2)*tau^-1 *j, -sqrt(2)*tau*i, 0], [-sqrt(2)*tau*k, 0, sqrt(2)*tau^-1*i]])
B3P5 = B53P.transpose()
B54P = Matrix([ [0, (1-3*sqrt(5))*i, (1+3*sqrt(5))*j, -2*k], 
               [-2*sqrt(10)*i, 0, sqrt(2)*(3+sqrt(5))*k, sqrt(2)*(3-sqrt(5))*j], 
               [0, -sqrt(3)*(1+sqrt(5))*i, sqrt(3)*(1-sqrt(5))*j, 2*sqrt(15)*k], 
               [2*sqrt(10)*k, -sqrt(2)*(3+sqrt(5))*j, -sqrt(2)*(3-sqrt(5))*i, 0], 
               [2*sqrt(10)*j, -sqrt(2)*(3-sqrt(5))*k, 0, -sqrt(2)*(3+sqrt(5))*i]])
#NB: There was a typo in the (4,3) entry of Sutcliffe [it's -sqrt(2)(3-sqrt(5))i, not sqrt(2)(3-sqrt(5))i]
B4P5 = B54P.transpose()
B4P3P = Matrix([ [i,j,k], [0, tau*k, tau^-1*j], [tau^-1*k, 0, tau*i], [tau*j, tau^-1*i, 0]])
B3P4P = B4P3P.transpose()

B53 = Matrix([[i, (-2/(sqrt(5) + 1) - 2)*j, (1/2*sqrt(5) + 1/2)*k],
              [0, (1/2*sqrt(2)*(sqrt(5) + 1))*k,(1/2*sqrt(2)*(sqrt(5) + 1))*j],
              [(2/3*sqrt(3) + sqrt(5/3))*i,  (1/6*sqrt(3) - 1/2*sqrt(5/3))*j,
                    (-5/6*sqrt(3) - 1/2*sqrt(5/3))*k],
              [(-sqrt(5/2) - 1/2*sqrt(2))*j,(-sqrt(5/2) - 1/2*sqrt(2))*i, 0],
              [(-sqrt(5/2) - 1/2*sqrt(2))*k, 0, (-sqrt(5/2) - 1/2*sqrt(2))*i]])
B35 = B53.transpose()
# All skew-symm, so don't put on diagonal
B3 = Matrix([ [0, k, -j], [-k, 0, i], [j, -i, 0]])
B3P = B3P4P*B4P3P
B4P = projim(B4P3P*B3P4P)
B5 = projim(B53P*B3P5)
B1 = identity_matrix(1)

# We have the following relations between these matrices, which I'll state in terms of vars
# This is in terms of imaginary parts; e.g. x4p*x43p = -x4p3p means that Im(B4P*B43P) = -Im(B4P3P)
R.<x4p2r,x4p2i,x24pr,x24pi,x24p,x4p2,x13,x31,x53p,x3p5, x4p5,x4p3p,x53,x3,x3p,x4p,x5,x1,x3p4p,x54p, x35> = FreeAlgebra(SR,21)
rels = [
    x4p * x4p - 2*x4p, x4p * x4p3p + x4p3p, x4p * x4p5 + x4p5,
    x4p3p * x3p5 - 1/2*x4p5, x4p3p * x3p4p - x4p, x4p3p * x3p + 4*x4p3p,
    x4p5 * x53p + 20*x4p3p, x4p5 * x53, x4p5 * x54p - 40*x4p,
    x4p5 * x5 + 4*x4p5, x5 * x5 - 2*x5, x5 * x53 - 6*x53,
    x5 * x53p + 4*x53p, x5 * x54p + 4*x54p, x54p * x4p5 - 16*x5,
    x54p * x4p + x54p, x54p * x4p3p - 8*x53p, x53p * x3p + 4*x53p,
    x53p * x3p5 - x5, x53p * x3p4p + 1/2*x54p, x53 * x35 + 1/2*(3+sqrt(5))*x5,
    x53 * x3 - x53, x53 * x31, x3p * x3p, x3p * x3p5 + 4*x3p5,
    x3p5 * x53p, x3p5 * x53, x3p5 * x5 + 4*x3p5,
    x3p5 * x54p - 20*x3p4p, x3p4p * x4p3p, x3p4p * x4p + x3p4p,
    x3p4p * x4p5 + 8*x3p5, x35 * x53p, x35 * x54p, x35 * x5 - 6*x35,
    x35 * x53 + 5/3*(3+sqrt(5))*x3, x31 * x1 - x31, x31 * x13 - x3,
    x3 * x3 + x3, x3 * x35 - x35, x3 * x31 + 2*x31,
    x1 * x13 - x13, x13 * x35, x35 * x54p, x13*x31, x1 * x1
]


# Force sage to do some evaluations it otherwise wouldn't
def substitute2(pairs, expr, do_eval=true):
    olds = expr
    for (a,b) in pairs:
        expr = expr.replace(a,b)
    if do_eval:
        return sage_eval(expr,locals=globals())
    else:
        return expr

pairs = [
    ("x24pr", "B24Pr"), ("x24pi", "B24Pi"), ("x4p2r", "B4P2r"), ("x4p2i", "B4P2i"),
    ("x24p", "B24P"), ("x4p2", "B4P2"), ("x4p3p", "B4P3P"), ("x3p4p", "B3P4P"),
    ("x3p5", "B3P5"), ("x53p", "B53P"), ("x4p5", "B4P5"), ("x54p", "B54P"),
    ("x4p^2", "B4P*B4P"), ("x3p^2", "B3P*B3P"), ("x5^2", "B5*B5"), ("x3^2", "B3*B3"),
    ("x1^2", "B1*B1"), ("x3p", "B3P"), ("x4p", "B4P"), ("x13", "B13"), ("x31", "B31"),
    ("x35", "B35"), ("x53", "B53"), ("x1", "B1"), ("x3", "B3"), ("x5", "B5"),
]

pairsr = [ (b,a) for a,b in pairs ]

# From the rels
supress_gen_messages = true
pairs2 = [
  ("x35*x53", "-(5/3*sqrt(5)+5)*B3"),
  ("x53*x35", "-(1/2*sqrt(5)+3/2)*B5")
]


for rel in rels:
    s = str(rel)
    if s.count('+') >= 2:
        if not supress_gen_messages:
            print("Couldn't auto generate rewrite rule for relation " + s + ", please add it manually")
        continue

    if s.count('+') == 0:
        lhs = s
        rhs = '0'
    else:
        lhs, rhs = s.split('+')

    if lhs.count('x') == 1 and lhs.count('^') == 0:
        lhs, rhs = rhs,lhs

    if rhs.count('*') > 0:
        rhsp = rhs.split('*')
        rhsp[0] = '(-1)*' + '(' + rhsp[0] + ')'
        rhsp2=[]
        for p in rhsp:
            if 'x' in p:
                rhsp2.append(p.replace('x', 'B').replace('p', 'P'))
            else:
                rhsp2.append(p)
        rhs = '*'.join(rhsp2)
    elif rhs != '0':
        rhs = "(-1)*(" + rhs.replace('x', 'B').replace('p', 'P') + ")"

    lhs = ''.join(lhs.split())
    rhs = ''.join(rhs.split())
    pairs2.append((lhs,rhs))

pairs2 = sorted(pairs2,reverse=true)


# Sanity check to verify rels:
do_sanity_check=false
if do_sanity_check:
    for rel in rels[:-1]:
        M = projim(substitute2(pairs,str(rel)))
        err = sum([ sum([ norm(evalf(a)) for a in b ]) for b in M ])
        print("Relation: " + str(rel) + "; error is " + str(err))

im_map = { (3,5): x35, (3,1): x31, (5,3): x53, (5,-4): x54p, (5,-3): x53p,
           (-3, 5): x3p5, (-3, -4): x3p4p, (-4,5): x4p5, (-4,-3): x4p3p,
          (1,3): x13, (5,5): x5, (-4,-4): x4p, (-3,-3): x3p, (3,3): x3, (1,1): x1}

var('b01 b02 b03 b04 b05 b06 b07 b08 b09 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22\
    c01 c02 c03 c04 c05 c06 c07 c08 c09 c10 c11 c12 c13 c14 c15 c16 c17 c18 c19 c20 c21 c22 \
    c23 c24 c25 c26 c27 c28 c29 c30 c31 c32 c33 c34 c35 c36 c37 c38 c39 c40')
# Build a big block matrix associated to this set of irreps
# Use a negative to indicate a primed rep; so [-3,-4] and [-3,-4,5,5] are Sutcliffe's
# Have to give L explicitly, it involves a choice of 2-diml rep
def assemble_matrix(L, Lc, irrep_dims):
    avail_vars_im = [c01, c02, c03, c04, c05, c06, c07, c08, c09,c10,c11,c12,c13,c14,c15,c16,c17,
                     c18,c19,c20,c21,c22,c23,c24,c25,c26,c27,c28, c29,c30,c31,c32,c33,c34,
                     c35,c36,c37,c38,c39,c40]
    avail_vars_re = [b01,b02,b03,b04,b05,b06,b07,b08,b09,b10,b11,b12,b13,b14,b15,
                     b16,b17,b18,b19,b20,b21,b22]
    cur_var_im = 0
    cur_var_re = 0
    im_vars_map = {}
    re_vars_map = {}
    blocks = []
    blocks.append(L)
    conj_blocks = []
    conj_blocks.append(Lc)
    for i, d in enumerate(irrep_dims):
        block = []
        conj_block = []
        for j, dd in enumerate(irrep_dims):
            im_part = 0
            im_part_conj = 0 
            re_part = 0
            if (d,dd) in im_map:
                if i < j:
                    im_vars_map[(i,j)] = avail_vars_im[cur_var_im]
                    im_vars_map[(j,i)] = im_vars_map[(i,j)]
                    # account for skew symmetry here
                    if d == dd:
                        im_vars_map[(j,i)] = -im_vars_map[(i,j)]
                    cur_var_im += 1
                
                # Never put an imaginary block on the diagonal, they are all skew-symmetric.
                if i != j: 
                    im_part = im_vars_map[(i,j)]*im_map[(d,dd)]
                    im_part_conj = -im_vars_map[(i,j)]*im_map[(dd,d)]
                    # skew symmetry
                    if d == dd:
                        im_part_conj = im_vars_map[(i,j)]*im_map[(dd,d)]
            if d == dd:
                if i <= j:
                    re_vars_map[(i,j)] = avail_vars_re[cur_var_re]
                    re_vars_map[(j,i)] = re_vars_map[(i,j)]
                    cur_var_re += 1
                re_part = re_vars_map[(i,j)]*1
            block.append(re_part+im_part)
            conj_block.append(re_part+im_part_conj)
        blocks.append(block)  
        conj_blocks.append(conj_block)

    print("Used " + str(cur_var_re) + " real variable(s) and " + str(cur_var_im) + " imaginary variable(s)")
    return matrix(blocks), matrix(conj_blocks).transpose(), \
            [*avail_vars_re[0:cur_var_re], *avail_vars_im[0:cur_var_im]]

var('a1 a2 a3 a4 a5 a6 a7 a8')
L_vars = [a1, a2, a3, a4, a5, a6, a7, a8]
cur_var_L = 0


c = char(REP_DIM)
#print("********************************* WARNING ********************************")
#print("currenly running in a mode that ignores input")REP_DIM = 8
#REP_MUL = 1
dims_pre = decomp2(c)
dims = []
for dim in dims_pre:
	dims.extend([dim]*REP_MUL)
print("Building icosahedral instanton for dimension " + str(REP_DIM))
print("Decomposition of the restricted SU(2) rep of this dimension: V = " + \
        '+'.join([ str(x) if x>0 else str(abs(x))+'\'' for x in dims ]))
if q == -2 and decomp(char(REP_DIM))[-2] == 0:
    print("WARNING: This data is singular because q=2' but the rep has no 4' in it. Do you want q=2?")
if q == 2 and decomp(char(REP_DIM))[0]+decomp(char(REP_DIM))[2] == 0:
    print("WARNING: This data is singular because q=2 but the rep has no 1 or 3 in it. Do you want q=2'?")

L_maps = {(-2, -4): x24pr+x24pi, (-4,-2): -x4p2r+x4p2i,
          (2, 3): x13, (3, 2): x31,
          (2, 1): R.one(), (1,2): R.one()}


L = []
Lc = []
for dim in dims:
    if (q, dim) in L_maps:
        L.append(L_vars[cur_var_L]*L_maps[(q,dim)])
        Lc.append(-L_vars[cur_var_L]*L_maps[(dim,q)])
        cur_var_L += 1
    else:
        L.append(0)
        Lc.append(0)

Mhat, Mhatdag, variables = assemble_matrix(L,Lc,dims)
variables.extend(L_vars[0:cur_var_L])

Delta = Mhatdag*Mhat

def im_part(x):
    one = (1+x1).trailing_term().support()[0]
    if x == 0:
        return x
    elif x.trailing_term().support()[0] == one:
        return x-x.trailing_term()
    else:
        return x

S = R.monoid()
part1 = S.gens()[0]*S.gens()[3]
part2 = S.gens()[1]*S.gens()[2]
part3 = S.gens()[1]*S.gens()[3]
assert(R(part1) == x4p2r*x24pi)
assert(R(part2) == x4p2i*x24pr)
assert(R(part3) == x4p2i*x24pi)

def special_L_rewrite(x):
    if x.coefficient(part1) != 0:
        if x.coefficient(part1) == -x.coefficient(part2) and \
            x.coefficient(part2) == x.coefficient(part3):
            return x-x.coefficient(part1)*R(part1) - x.coefficient(part2)*R(part2)  \
                    - x.coefficient(part3)*R(part3) + x.coefficient(part1)*x4p
        else:
            return x
    else:
        return x

def algebra_rewrite(x):
    return substitute2(pairsr, str(substitute2(pairs2, str(x), false)))


# Sage won't rewrite this for us, so we have to do it manually
Delta = Matrix([ [ algebra_rewrite(special_L_rewrite(x)) for x in y ] for y in Delta ])

eqns = [ x.coefficients()[0] for x in [ im_part(x) for x in
            flatten([ [ x for x in y ] for y in Delta ]) if x != 0 ] if x != 0 ]
eqns_cleaned = []
for e in eqns:
    if -e in eqns:
        continue
    eqns_cleaned.append(e)
#eqns = eqns_cleaned

def good_substitute2(expr, **kwargs):
	exprs = str(expr)
	print(exprs)
	for (key,val) in sorted(kwargs.items(), key=lambda x: x[0], reverse=true):
		exprs = exprs.replace(key,'(' + str(val) + ')')

	exprs = exprs.replace(')I', ')*I')
	return sage_eval(exprs,locals=globals())

def good_substitute(expr, **kwargs):
	if type(expr) != type(B53P):
		return expr
	return matrix([ [
		good_substitute2(x, **kwargs) for x in r
	] for r in expr ])

def build_matrix(**kwargs):
	# We need to turn every entry of Mhat into a block matrix, and assemble them with the block_matrix command
	# In short, this means going through every entry of Mhat and replacing x->B, p->P, while also replacing variables
	# and turning 0 entries into zero matrices
	# We can subtract the block matrix of x after
	
	var('y1 y2 y3 y4')
	y = y1 + y2*i + y3*j + y4*k
	z = lambda a,b: zero_matrix(abs(a),abs(b))
	Id = lambda a: identity_matrix(a)

	y_blocks = block_matrix([ [z(1,REP_DIM*REP_MUL)], [-y*Id(REP_DIM*REP_MUL)] ])

	M_blocks = []
	for (row,d1) in zip(Mhat[1:], dims):
		M_blockss = []
		for (el,d2) in zip(row, dims):
			subs = good_substitute(sage_eval(str(el).replace('x','B').replace('p','P'),
                                    locals=globals()).substitute(**kwargs), **kwargs)
			if subs == 0:
				M_blockss.append(z(d1,d2))
			else:
				M_blockss.append(subs)
		M_blocks.append(M_blockss)

	L_blocks = []
	for (el,d1) in zip(Mhat[0], dims):
		if el == 0:
			L_blocks.append(z(1,d1))
		else:
			L_blocks.append(good_substitute(sage_eval(str(el).replace('x','B').replace('p','P'),
                             locals=globals()), **kwargs))

	Mhat2 = block_matrix([L_blocks, *M_blocks]) - y_blocks
	print(Mhat2)
	Mhat2D = conjugate_transpose(Mhat2)
	D = Mhat2D * Mhat2
	Rr = projreal(evalfm(D))
	return D, Rr

def C_print(mat):
	for a in mat:
		print('{' + ','.join([ str(x).replace('y1^2 + y2^2 + y3^2 + y4^2', 'norm').replace('y','x') for x in a ]) + '},')


points = [
	[0, 2-tau/sqrt(6-3*tau), 1/sqrt(6-3*tau), [1, 0, 0], [0, -1, 2-tau], "dodecahedron"],
	[(tau-1)/sqrt(3-tau), 0, 1/sqrt(3-tau), [0, 1, 0], [-1, 0, tau-1], "icosahedron"],
	[1, 0, 0, [0,1,0], [0,0,1], "icosidodecahedron"],
	[-tau/sqrt(tau+2), 1/sqrt(tau+2), 0, [0, 0, 1], [-1, -tau, 0], "buckyball"],
]

def test_points(Rr):
	n = len(Rr.columns())
	M = Rr - Rr[0][0]*identity_matrix(n)
	max_ev = 0
	the_point = None
	for point in points:
		Mm = matrix([ [ x.n() for x in y] for y in list(M.substitute(y2=point[0],y3=point[1],y4=point[2])) ])
		ev = sorted([x.n() for x in Mm.eigenvalues()])[-1]
		print(ev)
		if ev > max_ev:
			max_ev = ev
			the_point = point
		
	Mm = M.substitute(y2=the_point[0],y3=the_point[1],y4=the_point[2])
	vs = Mm.eigenvectors_right()
	vs = sorted(vs, key = lambda x: x[0].n())
	v = vs[-1]
	v[0].n()
	eigenvec = v[1][0]
	M1 = M.substitute(y2=the_point[3][0], y3=the_point[3][1], y4=the_point[3][2])
	M2 = M.substitute(y2=the_point[4][0], y3=the_point[4][1], y4=the_point[4][2])
	if abs((M1*eigenvec).dot_product(eigenvec).n()) < 0.001 and abs((M2*eigenvec).dot_product(eigenvec).n()) < 0.001:
		# check its a maximum, not any other kind of critical point
		print("maxmial points have shape {}".format(the_point[-1]))
			
print("ADHM equations constructed. See Mhat for the general ADHM data form, Delta for Mhat^dag*Mhat, and eqns for the ADHM equations.")
print(str(len(eqns)) + " equations were found. You may be able to solve them with solve(eqns, *variables) for small dimensions.")
print("Or, you might try making some guesses to reduce the solution space. Do something like the following to set some translation parameters to 0:")
print("\teqns = [ x.substitute(b1=0,b2=0,b3=0) for x in eqns ] ")
print("Once you're ready, use D, Rr = build_matrix(b1=..., c1=...) to get a pair of matrices, Delta(x), and R(x) = Re(Delta(x)^* Delta(x)).")
print("You can use C_print(Rr) to print this in a format ready to copy and paste into a C program.")