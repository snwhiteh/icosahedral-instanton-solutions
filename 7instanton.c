
#include <math.h>

#define abs(x) (x < 0 ? -x : x)

#ifdef BUILD_DLL
#define EXPORT __declspec(dllexport)
#else
#define EXPORT __declspec(dllimport)
#endif

const double EPS = 1e-10; // for checking 0

#define N 7

double A[N][N];
double det;

void sswap(int i, int j) {
    for (int k = 0; k < N; k++) {
        double tmp = A[i][k];
        A[i][k]= A[j][k];
        A[j][k] = tmp;
    }
    det *= -1.0;
}

void add(int i, int j, double k) {
    for (int q = 0; q < N; q++) {
        A[j][q] += k * A[i][q];
    }
}

void scale(int i, double k) {
    for (int j = 0; j < N; j++) {
        A[i][j] *= k;
    }
    det *= 1.0/k;
}

EXPORT double __stdcall R(double x1, double x2, double x3, double x4) {
    double norm = x1*x1 + x2*x2 + x3*x3 + x4*x4;
    det = 1.0;
	double B[N][N] = {{norm + 4.00000000000000,0.000000000000000,0.000000000000000,-2.00000000000000*x2,0.000000000000000,-1.23606797749979*x4,-3.23606797749979*x3},
{0.000000000000000,norm + 4.00000000000000,0.000000000000000,-2.00000000000000*x3,-3.23606797749979*x4,0.000000000000000,-1.23606797749979*x2},
{0.000000000000000,0.000000000000000,norm + 4.00000000000000,-2.00000000000000*x4,-1.23606797749979*x3,-3.23606797749979*x2,0.000000000000000},
{-2.00000000000000*x2,-2.00000000000000*x3,-2.00000000000000*x4,norm + 4.00000000000000,0.000000000000000,0.000000000000000,0.000000000000000},
{0.000000000000000,-3.23606797749979*x4,-1.23606797749979*x3,0.000000000000000,norm + 4.00000000000000,0.000000000000000,0.000000000000000},
{-1.23606797749979*x4,0.000000000000000,-3.23606797749979*x2,0.000000000000000,0.000000000000000,norm + 4.00000000000000,0.000000000000000},
{-3.23606797749979*x3,-1.23606797749979*x2,0.000000000000000,0.000000000000000,0.000000000000000,0.000000000000000,norm + 4.00000000000000}};
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            A[i][j] = B[i][j];
        }
    }

    for (int i = 0; i < N; i++) {
        if (A[i][i] == 0) {
            for (int j = i+1; j < N; j++) {
                if (abs(A[j][i]) > EPS) {
                    sswap(i, j);
                    break;
                }
            }
        }
        if (abs(A[i][i]) <= EPS) {
            return 0;
        }

        double k = 1.0/A[i][i];
        
        scale(i, k);
        for (int j = 0; j < N; j++) {
            if (i != j) {
                add(i, j, -A[j][i]);
            }
        }
    }
    return det;
}

EXPORT double __stdcall logR(double x1, double x2, double x3, double x4) {
    return log(R(x1,x2,x3,x4));
}

EXPORT double __stdcall D2logR(double x1, double x2, double x3, double x4) {
    double h = 0.001;
    double d2x1 = (logR(x1+h,x2,x3,x4)-2*logR(x1,x2,x3,x4)+logR(x1-h,x2,x3,x4))/(h*h);
    double d2x2 = (logR(x1,x2+h,x3,x4)-2*logR(x1,x2,x3,x4)+logR(x1,x2-h,x3,x4))/(h*h);
    double d2x3 = (logR(x1,x2,x3+h,x4)-2*logR(x1,x2,x3,x4)+logR(x1,x2,x3-h,x4))/(h*h);
    double d2x4 = (logR(x1,x2,x3,x4+h)-2*logR(x1,x2,x3,x4)+logR(x1,x2,x3,x4-h))/(h*h);
    return d2x1 + d2x2 + d2x3 + d2x4;
}

EXPORT double __stdcall D2D2logR(double x1, double x2, double x3, double x4) {
    double h = 0.001;
    double d2x1 = (D2logR(x1+h,x2,x3,x4)-2*D2logR(x1,x2,x3,x4)+D2logR(x1-h,x2,x3,x4))/(h*h);
    double d2x2 = (D2logR(x1,x2+h,x3,x4)-2*D2logR(x1,x2,x3,x4)+D2logR(x1,x2-h,x3,x4))/(h*h);
    double d2x3 = (D2logR(x1,x2,x3+h,x4)-2*D2logR(x1,x2,x3,x4)+D2logR(x1,x2,x3-h,x4))/(h*h);
    double d2x4 = (D2logR(x1,x2,x3,x4+h)-2*D2logR(x1,x2,x3,x4)+D2logR(x1,x2,x3,x4-h))/(h*h);
    return d2x1 + d2x2 + d2x3 + d2x4;
}
