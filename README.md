Any of the .c files in this repository can be compiled as a shared lib and then inputted into MAPLE using the Native C functionality it possesses.
The (proportional) charge density of the instanton in question can be obtained using the C function D2D2logR, and then plotted using Maple implicit plotting routines after taking a slice through x4 = 0, as described in Chapter 6 of my thesis.

The instanton builder file is an interactive Sage program to get the symmetric ADHM equations for instantons of icosahedral symmetry. Load the file with Sage and follow the directions provided.

The gen_sols program implements the computational procedure described in the proof of Theorem 5.6.2.
